# Users
User
 id: number
 name: string;
 email: string | null
 isActive: boolean
 isAdmin: boolean

Trainer
 id: number
 user: User
 photo: string
 trainings: Training[]
 position: string
 department: string

StudentGroup
 id: number
 slug: "shop" | "office" | "all"
 title: string

# Events
Event
 id: number
 eventDateStart: date
 eventDateEnd: date
 training: Training
 studentsCount: number
 trainers: Trainer[]
 createdAt: date
 author: User
 isActive: boolean
 isCompleted: boolean

# Trainings
Training
 id: number
 title: string
 studentGroup: StudentGroup
 trainers: Trainer[]

# Tests
Test
 id: number
 trainingId: number
 questions: Question[]
 created: date

Question
 id: number
 position: number
 title: string
 testId: number
 variants: QuestionVariant[]

QuestionVariant
 id: number
 position: number
 questionId: number
 title: string
 isCorrect: boolean

# Tests answers
TestAnswer
 id: number
 testId: number
 created: date
 answers: []
 result: number
 correctCount: number

QuestionAnswer
 id: number
 testAnswerId: number
 questionId: number
 questionVariant: id
 isCorrect: boolean

# Fedback

Feedback
 id: number
 trainingId: number
 name: string
 created: date
 positive: string
 constructive: string
 score: ScoreEnum
 trainersId: Trainer[]

TrainerFeedback
 id: number
 trainerId: number
 feedbackId: number
 score: ScoreEnum
 comment: string

 ScoreEnum = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10