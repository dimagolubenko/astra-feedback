// Core
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { HomePageComponent } from './pages/components/home-page/home-page.component';
import { RegisterPageComponent } from './pages/components/register-page/register-page.component';
import { UserListComponent } from './users/components/user-list/user-list.component';
import { LoginPageComponent } from './pages/components/login-page/login-page.component';

const routes: Routes = [
  { path: "home", component: HomePageComponent },
  { path: "users", component: UserListComponent },
  { path: "login", component: LoginPageComponent },
  { path: "register", component: RegisterPageComponent },
  { path: "", redirectTo: "/home", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
