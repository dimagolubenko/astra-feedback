import { Component, EventEmitter, Output } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { CreateUserInterface } from "../../create-user.interface";

@Component({
  selector: "register-form",
  templateUrl: "./register-form.component.html",
  styleUrls: ["./register-form.component.scss"]
})
export class RegisterFormComponent {
  registerForm = this.fb.group({
    email: ["", [Validators.required, Validators.minLength(5)]],
    password: ["", [Validators.required, Validators.minLength(5)]]
  })

  @Output() submitEvent = new EventEmitter<CreateUserInterface>();

  constructor(private fb: FormBuilder) {}

  onSubmit() {
    console.log('form data: ', this.registerForm.value);
    this.submitEvent.emit({
      email: this.registerForm.get("email")?.value,
      password: this.registerForm.get("password")?.value
    })
  }
}