import { Component, EventEmitter, Output } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { LoginInterface } from "../../login.interface";

@Component({
  selector: "login-form",
  templateUrl: "./login-form.component.html",
  styleUrls: ["./login-form.component.scss"]
})
export class LoginFormComponent {
  loginForm = this.fb.group({
    email: ["", [Validators.required, Validators.minLength(5)]],
    password: ["", [Validators.required, Validators.minLength(5)]]
  })

  @Output() submitEvent = new EventEmitter<LoginInterface>();

  constructor(private fb: FormBuilder) {}

  onSubmit() {
    this.submitEvent.emit({
      email: this.loginForm.get("email")?.value,
      password: this.loginForm.get("password")?.value
    })
  }
}