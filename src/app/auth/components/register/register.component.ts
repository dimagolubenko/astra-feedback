import { Component } from "@angular/core";
import { AuthService } from "../../auth.service";
import { CreateUserInterface } from "../../create-user.interface";

@Component({
  selector: "register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"]
})
export class RegisterComponent {
  constructor(private authService: AuthService) {}

  async handleRegister(createUserInterface: CreateUserInterface) {
    const userData = await this.authService.register(createUserInterface);
    console.log("userData", userData);
  }
}