import { Component } from "@angular/core";

import { AuthService } from "../../auth.service";
import { LoginInterface } from "../../login.interface";

@Component({
  selector: "login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent {
  constructor(private authService: AuthService) {}

  async handleLogin(loginInterface: LoginInterface) {
    const userData = await this.authService.login(loginInterface);
    console.log("userData", userData);
  }
}