// Core
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

// Material
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

// Components
import { LoginFormComponent } from "./components/login-form/login-form.component";
import { LoginComponent } from "./components/login/login.component";
import { RegisterFormComponent } from "./components/register-form/register-form.component";
import { RegisterComponent } from "./components/register/register.component";

// Services
import { AuthService } from "./auth.service";

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FlexLayoutModule
  ],
  declarations: [RegisterComponent, RegisterFormComponent, LoginFormComponent, LoginComponent],
  providers: [AuthService],
  exports: [RegisterComponent, LoginComponent]
})
export class AuthModule {}