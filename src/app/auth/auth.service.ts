// Core
import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/compat/auth";

// Interfaces
import { CreateUserInterface } from "./create-user.interface";
import { LoginInterface } from "./login.interface";

@Injectable()
export class AuthService {
  constructor(public fireAuth: AngularFireAuth) {}

  async register({email, password}: CreateUserInterface) {
    try {
      const { user } = await this.fireAuth.createUserWithEmailAndPassword(email, password);
      if(user) {
        return { email: user.email, refreshToken: user.refreshToken, uid: user.uid };
      } else {
        throw new Error('User not register');
      }
    } catch(error) {
      throw new Error((error as Error).message)
    }
  }

  async login({ email, password }: LoginInterface) {
    try {
      const { user } = await this.fireAuth.signInWithEmailAndPassword(email, password);
      if(user) {
        return { email: user.email, refreshToken: user.refreshToken, uid: user.uid };
      } else {
        throw new Error('User not register');
      }
    } catch(error) {
      throw new Error((error as Error).message);
    }
  }
}