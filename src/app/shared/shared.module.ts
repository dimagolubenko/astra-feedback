import { NgModule } from "@angular/core";
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import { FlexLayoutModule } from "@angular/flex-layout";
import { CommonModule } from "@angular/common";

import { AuthModule } from "../auth/auth.module";
import { LayoutMainComponent } from "./components/layout-main/layout-main.component";
import { RouterModule } from "@angular/router";
import { HeaderMainComponent } from "./components/header-main/header-main.component";

@NgModule({
    imports: [
        CommonModule,
        MatToolbarModule,
        MatIconModule,
        FlexLayoutModule,
        RouterModule,
        AuthModule
    ],
    declarations: [LayoutMainComponent, HeaderMainComponent],
    providers: [],
    exports: [LayoutMainComponent]
})
export class SharedModule {}