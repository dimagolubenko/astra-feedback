import { Component } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/compat/auth";

@Component({
  selector: "header-main",
  templateUrl: "./header-main.component.html",
  styleUrls: ["./header-main.component.scss"]
})
export class HeaderMainComponent {
  constructor(public auth: AngularFireAuth) { }
}