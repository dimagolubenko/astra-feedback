import { Component } from "@angular/core";

@Component({
    selector: "layout-main",
    templateUrl: "./layout-main.component.html",
    styleUrls: ["./layout-main.component.scss"]
})
export class LayoutMainComponent {}