import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Select, Store } from "@ngxs/store";
import { Observable } from "rxjs";
import { FetchUsersAsync } from "../../store/users.actions";
import { UsersState } from "../../store/users.state";
import { UserInterface } from "../../user.interface";
import { UsersService } from "../../users.service";

@Component({
    selector: "users",
    templateUrl: "./user-list.component.html",
    styleUrls: ["./user-list.component.scss"]
})
export class UserListComponent implements OnInit {
    // users$!: Observable<UserInterface[]>;
    newUserForm = new FormGroup({
        name: new FormControl(""),
        email: new FormControl(""),
        isActive: new FormControl(true),
        isAdmin: new FormControl(false)
    });

    @Select(UsersState.users) users$: Observable<UserInterface[]>;

    constructor(
        private usersService: UsersService,
        private store: Store
    ) {}

    ngOnInit(): void {
        // this.users$ = this.usersService.getUsers();
        this.store.dispatch(new FetchUsersAsync());
    }

    addUser() {
        this.usersService.addUser(this.newUserForm.value);
    }

    deleteUser(id: string) {
        if(id) {
            this.usersService.deleteUser(id);
        }
    }
}