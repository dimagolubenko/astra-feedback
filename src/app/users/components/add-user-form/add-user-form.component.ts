import { Component } from "@angular/core";

@Component({
    selector: "add-user-form",
    templateUrl: "./add-user-form.component.html",
    styleUrls: ["./add-user-form.component.scss"]
})
export class AddUserFormComponent {}