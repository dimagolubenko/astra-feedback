import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { NgxsModule } from "@ngxs/store";
import { SharedModule } from "../shared/shared.module";
import { AddUserFormComponent } from "./components/add-user-form/add-user-form.component";
import { UserListComponent } from "./components/user-list/user-list.component";
import { UsersState } from "./store/users.state";
import { UsersService } from "./users.service";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxsModule.forFeature([UsersState]),
        // App modules
        SharedModule
    ],
    declarations: [UserListComponent, AddUserFormComponent],
    providers: [UsersService],
    exports: [AddUserFormComponent]
})
export class UsersModule {}