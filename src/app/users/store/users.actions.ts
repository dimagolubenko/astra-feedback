import { UserInterface } from "../user.interface";

export class FetchUsersAsync {
    static readonly type = "[Users] Fetch Users Async";
}

export class StartFetchUsers {
    static readonly type = "[Users] Start Fetch Users";
}

export class StopFetchUsers {
    static readonly type = "[Users] Stop Fetch Users";
}