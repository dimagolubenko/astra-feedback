import { Injectable } from "@angular/core";
import { Action, Selector, State, StateContext } from "@ngxs/store";
import { tap } from "rxjs";
import { UserInterface } from "../user.interface";
import { UsersService } from "../users.service";
import { FetchUsersAsync, StartFetchUsers, StopFetchUsers } from "./users.actions";

interface UsersStateModel {
    entries: UserInterface[] | null;
    isLoading: boolean;
}

@State<UsersStateModel>({
    name: "users",
    defaults: {
        entries: null,
        isLoading: false
    }
})
@Injectable()
export class UsersState {
    constructor(private usersService: UsersService) {}

    @Action(StartFetchUsers)
    startFetchUsers({ setState, getState }: StateContext<UsersStateModel>) {
        return setState({...getState(), isLoading: true});
    }

    @Action(StopFetchUsers)
    stopFetchUsers({ setState, getState }: StateContext<UsersStateModel>) {
        return setState({...getState(), isLoading: false});
    }

    @Action(FetchUsersAsync)
    fetchUsersAsync({ patchState, getState }: StateContext<UsersStateModel>) {
        return this.usersService.getUsers().pipe(
            tap(entries => {
                patchState({
                    ...getState(),
                    entries
                })
            })
        )
    }

    @Selector()
    static users(state: UsersStateModel) {
        return state.entries;
    }
}