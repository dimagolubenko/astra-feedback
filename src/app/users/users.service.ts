import { Injectable } from "@angular/core";
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from "rxjs";

import { UserInterface } from "./user.interface";

@Injectable()
export class UsersService {
	constructor(private firestore: AngularFirestore) {}

	getUsers(): Observable<UserInterface[]> {
		return this.firestore.collection<UserInterface>("users").valueChanges({idField: "id"});
	}

	async addUser(newUser: UserInterface): Promise<UserInterface> {
		try {
			const docRef = await this.firestore.collection<UserInterface>("users").add(newUser);
			const docSnap =	await docRef.get();
			return ({
				...docSnap.data(),
				id: docSnap.id
			}) as UserInterface;
		} catch (error) {
			throw new Error(error as string);
		}
	}

	deleteUser(id: string): void {
		try {
			this.firestore.collection<UserInterface>("users").doc(id).delete();
		} catch (error) {
			throw new Error(error as string);
		}
	}
}