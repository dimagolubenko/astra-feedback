export interface UserInterface {
    id: string;
    name: string;
    email: string;
    isActive: boolean;
    isAdmin: boolean;
}