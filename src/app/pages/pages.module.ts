import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import {MatButtonModule} from '@angular/material/button';

import { HomePageComponent } from "./components/home-page/home-page.component";
import { UsersModule } from "../users/users.module";
import { RegisterPageComponent } from "./components/register-page/register-page.component";
import { AuthModule } from "../auth/auth.module";
import { LoginPageComponent } from "./components/login-page/login-page.component";

@NgModule({
    imports: [
        MatButtonModule,
        // App Modules
        SharedModule,
        UsersModule,
        AuthModule
    ],
    declarations: [HomePageComponent, RegisterPageComponent, LoginPageComponent]
})
export class PagesModule {}