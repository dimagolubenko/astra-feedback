// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    projectId: 'astra-734d4',
    appId: '1:705912656982:web:a54beea40e4f5cff3c57f7',
    storageBucket: 'astra-734d4.appspot.com',
    locationId: 'europe-west',
    apiKey: 'AIzaSyA4AcO-9g218oi0xwFd3SzxvtAsRL5wIBM',
    authDomain: 'astra-734d4.firebaseapp.com',
    messagingSenderId: '705912656982',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
